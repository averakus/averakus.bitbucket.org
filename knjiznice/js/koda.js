
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
 
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
function generirajDropdown(){
   if(!document.getElementById("alreadyDone")){
  var div = document.getElementById("generate"); 
  var select = document.createElement("select");
  select.className="form-control input-sm";
  select.id="selectionboi";
  div.appendChild(select);
  document.getElementById("generate").id="alreadyDone";
  var options = ["1", "2", "3"];
  for(var i = 0; i < options.length; i++) {
    var opt = options[i];
    var el = document.createElement("option");
    el.textContent = opt;
    el.value = opt;
    select.appendChild(el);
}
         document.getElementById("selectionboi").onchange = function()
    {
        if(this.value == "1")
        {
            //do this function
            generirajPodatke(1);
        }
        else if(this.value == "2")
        {
            //do this function
            generirajPodatke(2);
        }
        else if(this.value == "3")
        {
           generirajPodatke(3);
        }
    };
   }
   }

 
  function generirajPodatke(stPacienta) {
  console.log("irjgri");
  var ehrId = "";
  var ime;
  var priimek;
  var datumRojstva;
  var visina;
  var teza;
  var temperatura;
  
  switch(stPacienta)
  {
    case 1:
      ehrId = "5a6f455a-1d60-490d-9d58-21329f633bc3";
      ime = "Teodor";
      priimek = "Jovanovski";
      datumRojstva = "1999-12-11";
      visina= "181";
      teza= "67";
      temperatura="36.7";
      break;
    
    case 2:
      ehrId = "42646b25-01ab-4467-9532-3d7c232af1b5";
      ime = "Matea";
      priimek = "Shalala";
      datumRojstva = "1999-11-28";
      visina= "177";
      teza= "40";
      temperatura="31.0";
      break;
    
    case 3:
      ehrId = "d24ddeae-8bfd-4c71-bc9a-b27dc6c4678e";
      ime = "Eva";
      priimek = "Kraus";
      datumRojstva = "1999-06-08";
      visina= "165";
      teza= "70";
      temperatura="38.9";
      break;
      
  }
      $("#meritveVitalnihZnakovEHRid").val(ehrId);
      $("#kreirajIme").val(ime);
      $("#kreirajPriimek").val(priimek);
      $("#kreirajDatumRojstva").val(datumRojstva);
      $("#dodajVitalnoEHR").val(ehrId);
        $("#dodajVitalnoDatumInUra").val("2019-03-01T12:00Z");
        $("#dodajVitalnoTelesnaVisina").val(visina);
        $("#dodajVitalnoTelesnaTemperatura").val(temperatura);
      $("#dodajVitalnoTelesnaTeza").val(teza);
      $("#dodajVitalnoMerilec").val("");
    
     console.log(ehrId);
  return ehrId;
}

   
function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preveriTemperaturo(){
var temperatura= $("#dodajVitalnoTelesnaTemperatura").val();
        if (temperatura.length== 0)
    $("#TempSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in' style='margin-left:1.5em;'>Prosim vnesite temperaturo!");
      else
  {
    document.getElementById("temperatura").innerHTML = $("#dodajVitalnoTelesnaTemperatura").val();
    if($("#dodajVitalnoTelesnaTemperatura").val()<36.3)
      $("#TempSporocilo").html("<span class='obvestilo label label-danger fade-in' style='margin-left:1.5em;'>Prenizka temperatura. Poiščite pomoč pri zdravniku");
    else if($("#dodajVitalnoTelesnaTemperatura").val()> 37.4)
    $("#TempSporocilo").html("<span class='obvestilo label label-danger fade-in' style='margin-left:1.5em;'>Previsoka temperatura. Poiščite pomoč pri zdravniku");
    else
     $("#TempSporocilo").html("<span class='obvestilo label label-success fade-in' style='margin-left:1.5em;'>Normalna temperatura, zdrava temperatura");

  
  }
}
function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
  var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		   
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */
function preberiMeritveVitalnihZnakov() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				if (tip == "telesna temperatura") {
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				} else if (tip == "telesna visina") {
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna visina</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
  				else if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
	
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
});

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */



/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;
var obmocje;

var FRI_LAT = 46.057891;
var FRI_LNG = 14.510353;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */

window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [46.057891, 14.510353],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
  
  // Ročno dodamo glavno postajo Ljubljane
  

  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    pridobiPodatke();
    popup
      .setLatLng(e.latlng)
      .setContent("Izbrana točka:" + e.latlng.toString())
      .openOn(mapa);
      FRI_LAT = e.latlng.lat;
      FRI_LNG = e.latlng.lng;
      posodobiOznakeNaZemljevidu();
    //prikazPoti(latlng);
  }
  pridobiPodatke();
  mapa.on('click', obKlikuNaMapo);
  
  
});

/**
 * Za podano vrsto interesne točke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 * 
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */

                    
function pridobiPodatke() {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        var stil={"color": "#2525AA",
                 "weight": 5,
                  "opacity": 0.65}
        L.geoJSON(json, {
          style: stil,
          onEachFeature: function (feature, layer) {
            /**layer.bindPopup("<b>"+feature.properties.name+"</b><br>"+
            json["addr:street"]+" ");*/
            //console.log(feature.properties.name);
            if(feature.properties.hasOwnProperty('name'))
            layer.bindPopup("<b>"+feature.properties.name+"</b><br>"+
            feature.properties["addr:street"]+" "+
            feature.properties["addr:housenumber"]+", "+
            feature.properties["addr:city"]);
          }
        }).addTo(mapa);
 

    }
  };
  xobj.send(null);
  
}
/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah,
 * z dodatnim opisom, ki se prikaže v oblačku ob kliku in barvo
 * ikone, glede na tip oznake (FRI = rdeča, druge fakultete = modra in
 * restavracije = zelena)
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 * @param tip "FRI", "restaurant" ali "faculty"
 */
function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-green.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}


/**
 * Na podlagi podanih interesnih točk v GeoJSON obliki izriši
 * posamezne točke na zemljevid
 * 
 * @param jsonRezultat interesne točke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat) {
  var znacilnosti = jsonRezultat.features;

  for (var i = 0; i < znacilnosti.length; i++) {
    var jeObmocje = 
      typeof(znacilnosti[i].geometry.coordinates[0]) == "object";
    var opis = znacilnosti[i].properties.name;

    // pridobimo koordinate
    var lng = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][0] : 
      znacilnosti[i].geometry.coordinates[0];
    var lat = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][1] : 
      znacilnosti[i].geometry.coordinates[1];
    if (prikaziOznako(lng, lat))
      dodajMarker(lat, lng, opis, znacilnosti[i].properties.amenity);
  }
}
/**
 * Glede na vrednost radija območja izbriši oz. dodaj
 * oznake na zemljevid.
 */
function posodobiOznakeNaZemljevidu() {
  // FRI marker pustimo, ostale odstranimo
  prikaziObmocje();
}


/**
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 */
function prikaziOznako(lng, lat) {
  var radij = 3;
  if (distance(lat, lng, FRI_LAT, FRI_LNG, "K") >= radij) 
    return false;
  else
    return true;
}


/**
 * Na zemljevidu nariši rdeč krog z transparentnim rdečim polnilom
 * s središčem na lokaciji FRI in radijem. Območje se izriše 
 * le, če je na strani izbrana vrednost "Prikaz radija".
 */
function prikaziObmocje() {
    if (obmocje != null) mapa.removeLayer(obmocje);
    while(markerji.length>0) //večji od 1 zaradi FRI markerja
      {
        mapa.removeLayer(markerji[markerji.length-1]);
        markerji.pop();
      }
    obmocje = L.circle([FRI_LAT, FRI_LNG], {
      color: 'pink',
      fillColor: 'blue',
      fillOpacity: 0.10,
      radius: 3 * 1000
    }).addTo(mapa);
    dodajMarker(FRI_LAT, FRI_LNG,'','');
}


